package jpa.services;


import java.util.Collection;
import java.util.Set;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;
import jpa.entites.Action;
import jpa.entites.Compte;
import jpa.entites.LigneAction;


public class ServiceBanqueBean  {
	protected EntityManager em ;
	private EntityManagerFactory emf;
	
	public ServiceBanqueBean() {
		this.emf = Persistence.createEntityManagerFactory("appliBanque");
    this.em = emf.createEntityManager();
	}
	public EntityTransaction getT() { return this.em.getTransaction() ; }
	
	
	
	
	
	public void addCompte(int numeroCompte, String nomTitulaire, double soldeDepart)
			throws CompteDejaExistantException {
		try {
			this.getCompte(numeroCompte) ;
			throw new CompteDejaExistantException() ;
		} catch(CompteInconnuException e) {
		  Compte c=new Compte() ;
		  c.setNumero(numeroCompte); c.setSolde(soldeDepart);
		  c.setTitulaire(nomTitulaire);
		  em.persist(c);
		}
	}

	
	public void addAction(String nomAction, double taux)
			throws ActionDejaExistanteException {
		try {
			this.getAction(nomAction) ;
			throw new ActionDejaExistanteException() ;
		} catch(ActionInconnueException e) {
		  Action a=new Action() ;
		  a.setNom(nomAction); a.setTaux(taux);
		  em.persist(a);
		}

	}

	private Action getAction(String nomAction) throws ActionInconnueException {
		Action a= (Action) em.find(Action.class, nomAction) ;
		if (a==null) throw new ActionInconnueException() ;
		return a ;
	}

	
	public void crediterCompte(int numeroCompte, double montant)
			throws CompteInconnuException {
		Compte c = this.getCompte(numeroCompte) ;
		c.setSolde(c.getSolde()+montant);
	}

	
	public void debiterCompte(int numeroCompte, double montant)
			throws CompteInconnuException {
		Compte c = this.getCompte(numeroCompte) ;
		c.setSolde(c.getSolde()-montant);
	}

	
	public void virementVers(int numCompteDebit, int numCompteCredit,
			double montant) throws CompteInconnuException,
			ApprovisionnementException {
		try {
		  this.debiterCompte(numCompteDebit, montant);
		  this.crediterCompte(numCompteCredit, montant);
		  if (this.getCompte(numCompteDebit).getSolde()<0) { // marche ou pas (flush ??)
			  throw new ApprovisionnementException();
		  }
		} catch(CompteInconnuException e) {
			throw new CompteInconnuException() ;
		}
	}

	
	public void acheteActions(int numeroCompte, String nomAction, int nb)
			throws CompteInconnuException, ActionInconnueException,
			ApprovisionnementException {
		Compte c = this.getCompte(numeroCompte) ;
		Action a = this.getAction(nomAction) ;
		if (c.getSolde()< nb*a.getTaux()) {
			throw new ApprovisionnementException() ;
		} else
			c.setSolde(c.getSolde()-nb*a.getTaux()) ;
		boolean trouve=false ;
		for (LigneAction la : c.getLignesactions()) 
			if (la.getAction().getNom().equals(nomAction)) {
				trouve=true ; la.setNombre(la.getNombre()+nb);
			}
		if (!trouve) {
			LigneAction la = new LigneAction() ;
			la.setAction(a); la.setProprietaire(c) ; la.setNombre(nb);
			c.getLignesactions().add(la);
			em.persist(la);
		}
	}

	
	public void vendActions(int numeroCompte, String nomAction, int nb)
			throws CompteInconnuException, ActionInconnueException,
			ApprovisionnementException {
		Compte c = this.getCompte(numeroCompte) ;
		LigneAction la=null ;
		for (LigneAction vla : c.getLignesactions()) 
			if (vla.getAction().getNom().equals(nomAction)) {
				la=vla ; 
			}
		if (la != null) {
			if (nb > la.getNombre()) 
				throw new ApprovisionnementException() ;
			else {
				c.setSolde(c.getSolde()+nb*la.getAction().getTaux());
				la.setNombre(la.getNombre()-nb) ;
				if (la.getNombre()==0) em.remove(la);
			}
		}
	}

	
	public Set<LigneAction> getLignesActions(int numeroCompte)
			throws CompteInconnuException {
		Compte c = this.getCompte(numeroCompte) ;
		//em.refresh(c);
		return c.getLignesactions() ;
	}
	
	 public Compte getCompte(int numeroCompte) throws CompteInconnuException {
		Compte c= (Compte) em.find(Compte.class, numeroCompte) ;
		if (c==null) throw new CompteInconnuException() ;
		return c;
	}

	

	@SuppressWarnings("unchecked")
	
	public Collection<Action> getActions(int numeroCompte) throws CompteInconnuException {
		this.getCompte(numeroCompte); // test existence compte
		String requete="select a from Compte c join c.lignesactions la join action a where c.numero= :num" ;
		Query q = em.createQuery(requete) ;
		q.setParameter("num", numeroCompte) ;
		return q.getResultList();
	}
	
	

}
