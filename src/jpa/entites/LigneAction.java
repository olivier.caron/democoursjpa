package jpa.entites;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;


@Table (name="t_ligne_action") @Entity
public class LigneAction implements java.io.Serializable {
  private static final long serialVersionUID = 1L;
  
  public LigneAction() {}
  
  @Id @GeneratedValue private int id ;
  private int nombre ;
  @ManyToOne private Compte proprietaire ;
  @ManyToOne(fetch=FetchType.EAGER) private Action action ;
  
  
  public int getId() { return id; }
  public void setId(int id) { this.id = id; }

  public int getNombre() { return nombre; }
  public void setNombre(int nombre) {
	this.nombre = nombre;
  }
  
  public Compte getProprietaire() {
	return proprietaire;
  }
  public void setProprietaire(Compte proprietaire) {
	this.proprietaire = proprietaire;
  }
  
  public Action getAction() {
	return action;
  }
  public void setAction(Action action) {
	this.action = action;
  }

}
