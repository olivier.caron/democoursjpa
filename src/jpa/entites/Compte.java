package jpa.entites;

import java.io.Serializable ;
import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Compte implements Serializable {
	
	@Id private int numero ;
	private double solde ;
	private String titulaire ;
	@OneToMany(mappedBy="proprietaire", fetch=FetchType.EAGER) 
	private Set<LigneAction> lignesactions ;
	
	
	
	public int getNumero() { return numero; }

	public void setNumero(int numero) { 
		this.numero = numero;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public String getTitulaire() {
		return titulaire;
	}

	public void setTitulaire(String titulaire) {
		this.titulaire = titulaire;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Compte() {
		this.lignesactions=new java.util.HashSet<LigneAction>() ;
	}

	public Set<LigneAction> getLignesactions() {
		return lignesactions;
	}

	public void setLignesactions(Set<LigneAction> lignesactions) {
		this.lignesactions = lignesactions;
	}

	
	
	
}
