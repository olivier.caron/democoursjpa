package jpa.entites;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Action implements java.io.Serializable {
  private static final long serialVersionUID = 1L;
  
  public Action() {}
  
  @Id private String nom ;
  private double taux ;
  
  public String getNom() { return nom; }
  public void setNom(String nom) {
	this.nom = nom;
  }
  
  public double getTaux() { return taux; }
  public void setTaux(double taux) {
	this.taux = taux;
  }
  
  
}
