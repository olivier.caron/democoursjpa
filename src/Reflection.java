import java.lang.reflect.* ;
import java.beans.Beans ;
import java.util.ArrayList ;

public class Reflection {

  @SuppressWarnings("rawtypes")
  public static void main(String args[]) {
    ArrayList<Method> propBool=new ArrayList<Method>() ;
    Class[] paramType= new Class[] { Boolean.TYPE };
    try {
	Object obj = Beans.instantiate(null,args[0]) ;
	Class laClasse = obj.getClass() ;
	for (Method m :  laClasse.getMethods()) {
	  if (m.getName().startsWith("is") &&
	      m.getParameterTypes().length==0 &&
	      m.getReturnType().toString().equals("boolean")) {     
		  	System.out.println("getter method found : "+m.getName()) ;
		  	String setName="set"+m.getName().substring(2) ;
            try {           
	          Method methodeSet=laClasse.getMethod(setName,paramType) ;
	          propBool.add(methodeSet) ;
	          String propertyName = setName.substring(0,1).toLowerCase() + setName.substring(1);
		      System.out.println("boolean property found : "+propertyName) ;
            } catch(NoSuchMethodException e1) { 
            	System.out.println("No corresponding setter method found : "+setName) ;
            }
	    }
	}
	Object param[] =new Boolean[1] ;
	param[0]=new Boolean(true) ;
	for (Method m : propBool) {
	  System.out.println("invoke method "+m.getName()+" with true parameter") ;
	  m.invoke(obj,param) ;
	}
    } catch (ArrayIndexOutOfBoundsException e) {
	System.err.println("Erreur : java Reflection class name") ;
    } catch (SecurityException e) {
	System.err.println("exception raised...") ;
    } catch(IllegalAccessException e) {
	System.err.println("Access...") ;
    } catch(InvocationTargetException e) {
	System.err.println("Argument...") ;
    } catch(IllegalArgumentException e) {
	System.err.println("Argument...") ;
    } catch(java.io.IOException e) {
	System.err.println("IO...") ;
    } catch(ClassNotFoundException e) {
	System.err.println("class...") ;
    }
  }  
}
