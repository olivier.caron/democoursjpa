package client ;


import jpa.entites.*;
import jpa.services.*;

public class Main {
		
 public static void main(String[] args) { 
   
      ServiceBanqueBean service = new ServiceBanqueBean() ;
      
      service.getT().begin();
      System.out.println("création comptes Olivier et Paul");
      try {
				service.addCompte(1, "Olivier", 2000.0);
				service.getT().commit();
				System.out.println("compte Olivier de 2000 Euros créé");
			} catch (CompteDejaExistantException e) {
				System.err.println("compte Olivier existe déjà"); service.getT().rollback();
			}
      
      service.getT().begin();
      try {
				service.addCompte(2, "Paul", 300.0) ;
				service.getT().commit();
				System.out.println("compte Paul de 300 Euros créé");
			} catch (CompteDejaExistantException e) {
				System.err.println("compte Paul existe déjà"); service.getT().rollback();
			}
     
      
      service.getT().begin();
      System.out.println("créditer compte  Olivier ");
      try {
				service.crediterCompte(1, 1000.0);
				service.getT().commit();
				System.out.println("Compte  Olivier crédité de 1000 Euros");
			} catch (CompteInconnuException e) {
				System.err.println("Compte  Olivier inconnu"); service.getT().rollback();
			}
      
      
      service.getT().begin();
				try {
					service.virementVers(1, 2, 2500);
					service.getT().commit();
					System.out.println("Virement de 2500 Euros de Olivier vers Paul");
				} catch (CompteInconnuException e) {
					System.err.println("Compte  Olivier ou Paul inconnu");service.getT().rollback();
				} catch (ApprovisionnementException e) {
					System.err.println("Approvisionnement insuffisant");service.getT().rollback();
				}
				
			service.getT().begin();
      try {
				service.addAction("nintendo",46.5) ;
				System.out.println("action nintendo de 46,5 Euros créée");
			} catch (ActionDejaExistanteException e) {
				System.err.println("action nintendo existe déjà") ;
			}
      try {
				service.addAction("sega",12.0) ;
				System.out.println("action sega de 12 Euros créée");
			} catch (ActionDejaExistanteException e) {
				System.err.println("action sega existe déjà") ;
			}
      service.getT().commit();
       
      service.getT().begin();
      System.out.println("achat de 10 actions sega") ;
      try {
				service.acheteActions(1, "sega", 10);
				service.getT().commit();
			} catch (CompteInconnuException e) {
				System.err.println("compte inconnu") ; service.getT().rollback();
			} catch (ActionInconnueException e) {
				System.err.println("action inconnue") ;service.getT().rollback();
			} catch (ApprovisionnementException e) {
				System.err.println("approvisionnement insuffisant") ;service.getT().rollback();
			}
      
      service.getT().begin();
      System.out.println("achat de 3 actions nintendo") ;
      try {
      	service.acheteActions(1,  "nintendo", 3);
				service.getT().commit();
			} catch (CompteInconnuException e) {
				System.err.println("compte inconnu") ; service.getT().rollback();
			} catch (ActionInconnueException e) {
				System.err.println("action inconnue") ;service.getT().rollback();
			} catch (ApprovisionnementException e) {
				System.err.println("approvisionnement insuffisant") ;service.getT().rollback();
			}
      
      try {
      System.out.println("Actions de Olivier:"+service.getLignesActions(1).size()) ;
      service.getT().begin() ;
      
				for (LigneAction la : service.getLignesActions(1))
				System.out.println(la.getNombre()+" action(s) "+la.getAction().getNom()
							        +" au taux de "+la.getAction().getTaux()) ;
			} catch (CompteInconnuException e) {
				
				e.printStackTrace();
			}
      service.getT().commit();
      /*
      System.out.println("vente de 2 actions sega") ;
      service.vendActions(1, "sega", 2);
      System.out.println("Actions de Olivier:") ;
      for (LigneAction la : service.getLignesActions(1))
    	 System.out.println(la.getNombre()+" action(s) "+la.getAction().getNom()
    				             +" au taux de "+la.getAction().getTaux()) ;
      System.out.println("vente de 8 actions sega") ;
      service.vendActions(1, "sega", 8);
      System.out.println("Actions de Olivier:") ;
      for (LigneAction la : service.getLignesActions(1))
        System.out.println(la.getNombre()+" action(s) "+la.getAction().getNom()
    				             +" au taux de "+la.getAction().getTaux()) ;   	  
    	
  	 */
 }
}